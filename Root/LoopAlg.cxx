#include "TRTdEdxPID/LoopAlg.h"
#include "TRTFramework/TRTIncludes.h"

// this is needed to distribute the algorithm to the workers
ClassImp(LoopAlg)

SG::AuxElement::ConstAccessor<float> LoopAlg::acc_eProbabilityToT("eProbabilityToT");
SG::AuxElement::ConstAccessor<float> LoopAlg::acc_eProbabilityHT("eProbabilityHT");
SG::AuxElement::ConstAccessor<float> LoopAlg::acc_TRTdEdx("TRTdEdx");
SG::AuxElement::ConstAccessor<float> LoopAlg::acc_ToT_dEdx_noHT_divByL("ToT_dEdx_noHT_divByL");
SG::AuxElement::ConstAccessor<float> LoopAlg::acc_ToT_usedHits_noHT_divByL("ToT_usedHits_noHT_divByL");
SG::AuxElement::ConstAccessor<MOT>   LoopAlg::acc_MOT("msosLink");

SG::AuxElement::ConstAccessor<unsigned int> LoopAlg::acc_bitPattern("bitPattern");
SG::AuxElement::ConstAccessor<char>         LoopAlg::acc_gasType("gasType");
SG::AuxElement::ConstAccessor<int>          LoopAlg::acc_layer("layer");
SG::AuxElement::ConstAccessor<int>          LoopAlg::acc_strawlayer("strawlayer");
SG::AuxElement::ConstAccessor<int>          LoopAlg::acc_bec("bec");
SG::AuxElement::ConstAccessor<float>        LoopAlg::acc_tot("tot");
SG::AuxElement::ConstAccessor<float>        LoopAlg::acc_localX("localX");
SG::AuxElement::ConstAccessor<float>        LoopAlg::acc_HitZ("HitZ");
SG::AuxElement::ConstAccessor<float>        LoopAlg::acc_HitR("HitR");
SG::AuxElement::ConstAccessor<float>        LoopAlg::acc_rTrkWire("rTrkWire");
SG::AuxElement::ConstAccessor<float>        LoopAlg::acc_localPhi("localPhi");
SG::AuxElement::ConstAccessor<float>        LoopAlg::acc_localTheta("localTheta");

LoopAlg::LoopAlg() : TRTAnalysis() {}

LoopAlg::~LoopAlg() {}

EL::StatusCode LoopAlg::histInit() {
  ANA_CHECK_SET_TYPE(EL::StatusCode);

  histoStore()->createTH1F("h_averageMu", 30, 0, 30, "; #LT#mu#GT; Events");
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode LoopAlg::init() {
  ANA_CHECK_SET_TYPE(EL::StatusCode);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode LoopAlg::loop() {
  ANA_CHECK_SET_TYPE(EL::StatusCode);

  // Remove Duplicate Events
  if ( isDuplicate() || !passGRL() || hasZeroWeight() ) {
    return EL::StatusCode::SUCCESS;
  }

  m_avgMu = averageMu();
  m_weight = weight();
  histoStore()->fillTH1F("h_averageMu",averageMu(),weight());

  // retrieve the selected electrons
  auto elecs = electrons();

  // loop over electrons which pass selection and analysize the track
  for ( auto const& elec : elecs ) {
    auto track = getTrack(elec);
    if ( passTracking(track) ) {
      if ( track != nullptr ) {
        analyzeTrack(track);
        ClearBranches(); // clear on each electron track
      }
    }
  }
  return EL::StatusCode::SUCCESS;
}

void LoopAlg::analyzeTrack(const xAOD::TrackParticle* track) {
  // only analyze the track if pT > 5 GeV
  //if ( track->pt() > 5000 ) {
  m_track_Occ = getTrackOccupancy(track);
  m_track_pT  = track->pt();
  m_track_p   = track->p4().P();
  m_track_eta = track->eta();
  m_track_phi = track->phi();
  m_eProbabilityHT           = get(acc_eProbabilityHT,          track);
  m_eProbabilityToT          = get(acc_eProbabilityToT,         track);
  m_TRTdEdx                  = get(acc_TRTdEdx,                 track);
  m_ToT_dEdx_noHT_divByL     = get(acc_ToT_dEdx_noHT_divByL,    track);
  m_ToT_usedHits_noHT_divByL = get(acc_ToT_usedHits_noHT_divByL,track);
  auto truth = getTruthPtr(track);
  if ( truth != nullptr ) {
    float m = truth->m();
    m_truth_betaGamma = m_track_p/m;
    m_truth_pdgId     = truth->pdgId();
    m_truth_m         = m;
  }
  else {
    m_truth_betaGamma = -999;
    m_truth_pdgId     = -999;
    m_truth_m         = -999;
  }
  analyzeHits(track);
  FillBranches(); // fill ntuple variables
  //}
}

void LoopAlg::analyzeHits(const xAOD::TrackParticle* track) {
  if ( !acc_MOT.isAvailable(*track) ) {
    TRT::fatal("No MSOS called msosLink is available on track");
  }
  const MOT& measurementsOnTrack = acc_MOT(*track);
  int nHits_Ar = 0;
  int nHits_HT = 0;
  int nHits    = 0;
  for ( auto& msosItr : measurementsOnTrack ) {
    if ( !msosItr.isValid() ) continue;
    const xAOD::TrackStateValidation* msos = *msosItr;
    if ( msos->detType() != 3 ) continue; // trt hit
    auto type = msos->type(); // what kind of hit is this according to the track.
    if ( !msos->trackMeasurementValidationLink().isValid() ) continue;
    const TMV* driftCircle =  *(msos->trackMeasurementValidationLink());
    auto tot        = get(acc_tot,        driftCircle);
    if ( tot < 0.0001 ) continue;
    auto bitPattern = get(acc_bitPattern, driftCircle);
    auto gasType    = get(acc_gasType,    driftCircle);
    auto layer      = get(acc_layer,      driftCircle);
    auto strawlayer = get(acc_strawlayer, driftCircle);
    auto bec        = get(acc_bec,        driftCircle);

    int part = 0; // 0: barrel ... 1: ECA ... 2: ECB
    if ( std::abs(bec) == 2 ) {
      part = ( layer < 6 ) ? 1 : 2;
    }
    if ( bec < 0 ) part = -part;

    auto driftRadius = get(acc_localX,   driftCircle);
    auto TW          = get(acc_localX,   msos);
    auto HitZ        = get(acc_HitZ,     msos);
    auto HitR        = get(acc_HitR,     msos);
    auto rTrkWire    = get(acc_rTrkWire, msos);
    auto ZR          = TRT::getZRPosition(track, driftCircle);
    if ( HitZ != -999 && HitR != -999 && rTrkWire != -999 ) {
      TW = rTrkWire;
      ZR = ( part == 0 ) ? HitZ : HitR;
    }
    float trackTheta = track->theta();
    float trackPhi   = track->phi();
    float strawPhi   = get(acc_localPhi, msos);

    float LIS = 0.0;
    if ( part == 0 ) {
      LIS = 2*std::sqrt(4.0-rTrkWire*rTrkWire)*1.0/std::fabs(std::sin(trackTheta));
    }
    else {
      LIS = 2*std::sqrt(4.0-rTrkWire*rTrkWire)*1.0/
        std::sqrt(1.0-std::sin(trackTheta)*std::sin(trackTheta)*
                  std::cos(trackPhi-strawPhi)*std::cos(trackPhi-strawPhi));
    }

    bool isHT = (bitPattern & 131072);
    bool isAr = (gasType == 1 || gasType == 6);

    m_hit_type.push_back(type);
    m_hit_bec.push_back(bec);
    m_hit_layer.push_back(layer);
    m_hit_slayer.push_back(strawlayer);
    m_hit_isAr.push_back(isAr);
    m_hit_isHT.push_back(isHT);
    m_hit_totoL.push_back(tot/LIS);
    m_hit_L.push_back(LIS);
    m_hit_tot.push_back(tot);
    m_hit_zr.push_back(ZR);
    m_hit_tw.push_back(TW);
    m_hit_phi.push_back(strawPhi);
    m_hit_dr.push_back(driftRadius);

    if ( isHT ) nHits_HT++;
    if ( isAr ) nHits_Ar++;
    nHits++;
  } // loop over hits

  m_fracHT = (float)nHits_HT/(float)nHits;
  m_fracAr = (float)nHits_Ar/(float)nHits;
}

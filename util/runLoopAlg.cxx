#include "TRTdEdxPID/LoopAlg.h"
#include "TRTFramework/RunUtils.h"

int main(int argc, char *argv[])
{
    // Set up the job for xAOD access
    xAOD::Init().ignore();

    // Create our algorithm
    LoopAlg *alg = new LoopAlg();

    // Use helper to start the job
    TRT::runJob(alg, argc, argv);

    return 0;
}

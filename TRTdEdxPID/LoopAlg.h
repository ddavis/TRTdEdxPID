#ifndef TRTdEdxPID_LoopAlg_H
#define TRTdEdxPID_LoopAlg_H

#include "TRTFramework/TRTAnalysis.h"

#define GeV   1000.0
#define toGeV 0.0010

using MOT = std::vector<ElementLink<xAOD::TrackStateValidationContainer>>;
using TMV = xAOD::TrackMeasurementValidation;
using TruthLink_t = ElementLink<xAOD::TruthParticleContainer>;
static SG::AuxElement::ConstAccessor<TruthLink_t> acc_truthLink("truthParticleLink");

class LoopAlg : public TRTAnalysis {

private:

  static SG::AuxElement::ConstAccessor<float> acc_eProbabilityToT;
  static SG::AuxElement::ConstAccessor<float> acc_eProbabilityHT;
  static SG::AuxElement::ConstAccessor<float> acc_TRTdEdx;
  static SG::AuxElement::ConstAccessor<float> acc_ToT_dEdx_noHT_divByL;
  static SG::AuxElement::ConstAccessor<float> acc_ToT_usedHits_noHT_divByL;
  static SG::AuxElement::ConstAccessor<MOT>   acc_MOT;

  static SG::AuxElement::ConstAccessor<unsigned int> acc_bitPattern;
  static SG::AuxElement::ConstAccessor<char>  acc_gasType;
  static SG::AuxElement::ConstAccessor<int>   acc_layer;
  static SG::AuxElement::ConstAccessor<int>   acc_strawlayer;
  static SG::AuxElement::ConstAccessor<int>   acc_bec;
  static SG::AuxElement::ConstAccessor<float> acc_tot;
  static SG::AuxElement::ConstAccessor<float> acc_localX;
  static SG::AuxElement::ConstAccessor<float> acc_HitZ;
  static SG::AuxElement::ConstAccessor<float> acc_HitR;
  static SG::AuxElement::ConstAccessor<float> acc_rTrkWire;
  static SG::AuxElement::ConstAccessor<float> acc_localPhi;
  static SG::AuxElement::ConstAccessor<float> acc_localTheta;

  ScalarBranchVariable<float> m_weight                   = ScalarBranchVariable<float>("weight",this);
  ScalarBranchVariable<float> m_avgMu                    = ScalarBranchVariable<float>("avgMu",this);
  ScalarBranchVariable<float> m_track_pT                 = ScalarBranchVariable<float>("track_pT",this);
  ScalarBranchVariable<float> m_track_Occ                = ScalarBranchVariable<float>("track_Occ",this);
  ScalarBranchVariable<float> m_track_p                  = ScalarBranchVariable<float>("track_p",this);
  ScalarBranchVariable<float> m_track_eta                = ScalarBranchVariable<float>("track_eta",this);
  ScalarBranchVariable<float> m_track_phi                = ScalarBranchVariable<float>("track_phi",this);
  ScalarBranchVariable<float> m_track_betaGamma          = ScalarBranchVariable<float>("track_betaGamma",this);
  ScalarBranchVariable<float> m_truth_betaGamma          = ScalarBranchVariable<float>("truth_betaGamma",this);
  ScalarBranchVariable<float> m_truth_m                  = ScalarBranchVariable<float>("truth_m",this);
  ScalarBranchVariable<int>   m_truth_pdgId              = ScalarBranchVariable<int>  ("truth_pdgId",this);
  ScalarBranchVariable<float> m_eProbabilityToT          = ScalarBranchVariable<float>("eProbabilityToT",this);
  ScalarBranchVariable<float> m_eProbabilityHT           = ScalarBranchVariable<float>("eProbabilityHT",this);
  ScalarBranchVariable<float> m_TRTdEdx                  = ScalarBranchVariable<float>("TRTdEdx",this); 
  ScalarBranchVariable<float> m_ToT_dEdx_noHT_divByL     = ScalarBranchVariable<float>("ToT_dEdx_noHT_divByL",this);
  ScalarBranchVariable<float> m_ToT_usedHits_noHT_divByL = ScalarBranchVariable<float>("ToT_usedHits_noHT_divByL",this);
  ScalarBranchVariable<float> m_fracHT                   = ScalarBranchVariable<float>("fracHT",this);
  ScalarBranchVariable<float> m_fracAr                   = ScalarBranchVariable<float>("fracAr",this);

  ScalarBranchVariable<bool>  m_recoFromZ                = ScalarBranchVariable<bool> ("recoFromZ",this);
  ScalarBranchVariable<bool>  m_recoFromJPsi             = ScalarBranchVariable<bool> ("recoFromJPsi",this);

  VectorBranchVariable<int>   m_hit_type   = VectorBranchVariable<int>  ("hit_type",  this);
  VectorBranchVariable<int>   m_hit_bec    = VectorBranchVariable<int>  ("hit_bec",   this);
  VectorBranchVariable<int>   m_hit_layer  = VectorBranchVariable<int>  ("hit_layer", this);
  VectorBranchVariable<int>   m_hit_slayer = VectorBranchVariable<int>  ("hit_slayer",this);
  VectorBranchVariable<bool>  m_hit_isAr   = VectorBranchVariable<bool> ("hit_isAr",  this);
  VectorBranchVariable<bool>  m_hit_isHT   = VectorBranchVariable<bool> ("hit_isHT",  this);
  VectorBranchVariable<float> m_hit_totoL  = VectorBranchVariable<float>("hit_totoL", this);
  VectorBranchVariable<float> m_hit_tot    = VectorBranchVariable<float>("hit_tot",   this);
  VectorBranchVariable<float> m_hit_tw     = VectorBranchVariable<float>("hit_tw",    this);
  VectorBranchVariable<float> m_hit_zr     = VectorBranchVariable<float>("hit_zr",    this);
  VectorBranchVariable<float> m_hit_phi    = VectorBranchVariable<float>("hit_phi",   this);
  VectorBranchVariable<float> m_hit_dr     = VectorBranchVariable<float>("hit_dr",    this);
  VectorBranchVariable<float> m_hit_L      = VectorBranchVariable<float>("hit_L",     this);

public:
  LoopAlg();
  virtual ~LoopAlg();
  virtual EL::StatusCode histInit();
  virtual EL::StatusCode init();
  virtual EL::StatusCode loop();

  void analyzeTrack(const xAOD::TrackParticle* track);
  void analyzeHits(const xAOD::TrackParticle* track);

  template<class T>
  T get(const SG::AuxElement::ConstAccessor<T>& ca, const xAOD::TrackParticle* track) const;

  template<class T>
  T get(const SG::AuxElement::ConstAccessor<T>& ca, const TMV* dc) const;
  template<class T>
  T get(const SG::AuxElement::ConstAccessor<T>& ca, const xAOD::TrackStateValidation* msos) const;

  const xAOD::TruthParticle* getTruthPtr(const xAOD::TrackParticle* track);

  bool isBarrelHit(const TMV* dc) const;
  bool isEndCapAHit(const TMV* dc) const;
  bool isEndCapBHit(const TMV* dc) const;
  ClassDef(LoopAlg, 1);
};

template<class T>
inline T LoopAlg::get(const SG::AuxElement::ConstAccessor<T>& ca, const xAOD::TrackParticle* track) const {
  if ( ca.isAvailable(*track) ) {
    return ca(*track);
  }
  else {
    return -999;
  }
}

template<class T>
inline T LoopAlg::get(const SG::AuxElement::ConstAccessor<T>& ca, const TMV* dc) const {
  if ( ca.isAvailable(*dc) ) {
    return ca(*dc);
  }
  else {
    TRT::fatal("Cannot retrieve a variable from the drift circle");
    return 0;
  }
}

template<class T>
inline T LoopAlg::get(const SG::AuxElement::ConstAccessor<T>& ca, const xAOD::TrackStateValidation* msos) const {
  if ( ca.isAvailable(*msos) ) {
    return ca(*msos);
  }
  else {
    return -999;
  }
}

inline const xAOD::TruthParticle* LoopAlg::getTruthPtr(const xAOD::TrackParticle* track) {
  const xAOD::TruthParticle *result = nullptr;
  if ( acc_truthLink.isAvailable(*track) ) {
    const TruthLink_t link = acc_truthLink(*track);
    if ( link.isValid() ) {
      result = *link;
    }
  }
  return result;
}

#endif // TRTdEdxPID_LoopAlg_H
